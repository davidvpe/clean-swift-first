//
//  UIView.swift
//  Clean Swift Project
//
//  Created by David on 6/27/18.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

extension UIView {
    func addSubviewForAutolayout(_ subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
    }
}
