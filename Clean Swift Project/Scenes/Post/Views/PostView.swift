//
//  PostView.swift
//  Clean Swift Project
//
//  Created by David on 6/27/18.
//  Copyright © 2018 David Velarde. All rights reserved.
//

import UIKit

class PostView: UIView {
    
    let tableView: UITableView
    
    override init(frame: CGRect) {
        tableView = UITableView()
        
        super.init(frame: frame)
        
        setupComponents() //Agregar la vista al parent view
        setupConstraints() // Setear constraints
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupComponents() {
        addSubviewForAutolayout(tableView)
    }
    func setupConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        
    }
}
